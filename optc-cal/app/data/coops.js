var coops = {
    4150: {
        name: 'Spring 24'
    },
    4171: {
        name: 'Summer 24-1'
    },
    4186: {
        name: 'Summer 24-2'
    },
    4202: {
        name: 'Summer 24-3'
    },
    4219: {
        name: 'Autumn 24-1'
    },
    4257: {
        name: 'Autumn 24-2'
    },
    4275: {
        name: 'Winter 24-1'
    },
    2799: {
        name: 'Wapol\'s Assault'
    },
    4307: {
        name: 'Winter 24-2'
    },
    4322: {
        name: 'Spring 25-1'
    },
}