var kizunas = {
    2518: {
        name: 'Zoro & Sanji',
        thumb: 2531,
        xch_id: 2518
    },
    2615: {
        name: 'Zoro & Sanji',
        thumb: 2531,
        xch_id: 2615
    },
    2630: {
        name: 'Zoro & Sanji',
        thumb: 2531,
        xch_id: 2630
    },
    2682: {
        name: 'Boa Hancock',
        xch_id: 2531
    },
    2531: {
        name: 'Zoro & Sanji'
    },
    2405: {
        name: 'Akainu',
        xch_id: 2531
    },
    2758: {
        name: 'HW Sabo'
    },
    2733: {
        name: 'Zoro & Sanji',
        thumb: 2531,
        xch_id: 2733
    },
    2794: {
        name: 'Xmas Shirahoshi',
        xch_id: 3330
    },
    2812: {
        name: 'O-Kiku'
    },
    2831: {
        name: 'Marco & Ace'
    },
    2854: {
        name: 'Koala',
        xch_id: 2831
    },
    2890: {
        name: 'Moria',
        xch_id: 2891
    },
    2920: {
        name: 'Big Mom',
        xch_id: 2831
    },
    2955: {
        name: 'Carrot'
    },
    2999: {
        name: 'Luffy & Hancock'
    },
    3036: {
        name: 'Kaido'
    },
    3052: {
        name: 'Buggy'
    },
    3098: {
        name: 'Sakazuki & Borsalino'
    },
    3124: {
        name: 'Kid'
    },
    3153: {
        name: 'Hawkins'
    },
    3207: {
        name: 'Bege & Pez'
    },
    3241: {
        name: 'King',
        xch_id: 3213
    },
    3276: {
        name: 'Doflamingo'
    },
    3303: {
        name: 'Katakuri'
    },
    3352: {
        name: 'Kuja Pirates'
    },
    3373: {
        name: 'MUGIWARA 56 Brook'
    },
    3397: {
        name: 'Jack',
        xch_id: [3450, 3470]
    },
    3419: {
        name: 'Ivankov',
        xch_id: [3490, 3511]
    },
    3532: {
        name: 'Blackbeard (~ 6am)'
    },
    3551: {
        name: 'Chopper (~ 6am)'
    },
    3572: {
        name: 'Orochi (~ 6am)'
    },
    3592: {
        name: 'Kuzan v2 6+ (~ 6am)'
    },
    3627: {
        name: 'Roger (~ 6am)'
    },
    3648: {
        name: 'Magellan & Hannyabal (~ 6am)'
    },
    3669: {
        name: 'Summer Luffy (~ 6am)'
    },
    3693: {
        name: 'Shiki 6+ (~ 6am)'
    },
    3721: {
        name: 'Uta 6+ (~ 6am)',
        xch_id: 3720
    },
    3739: {
        name: 'Blackbeard v2 6+ (~ 6am)'
    },
    3764: {
        name: 'Samurai Brook (~ 6am)'
    },
    3784: {
        name: 'Chopper 6+ (~ 6am)'
    },
    3807: {
        name: 'Doflamingo v2 (~ 6am)'
    },
    3828: {
        name: 'Inu & Neko 6+ (~ 6am)'
    },
    3848: {
        name: 'Garp (~ 6am)'
    },
    3868: {
        name: 'Barto & Hakuba 6+ (~ 6am)'
    },
    3898: {
        name: 'Alber (~ 6am)',
        xch_id: 3903
    },
    3920: {
        name: 'Luffy & Ace 6+ (~ 6am)'
    },
    3942: {
        name: 'Perona (~ 6am)'
    },
    3969: {
        name: 'Sanji & Judge 6+ (~ 6am)'
    },
    3991: {
        name: 'Yamato (~ 6am)'
    },
    4010: {
        name: 'Zephyr 6+ (~ 6am)'
    },
    4039: {
        name: 'Kid (OPCG) (~ 6am)'
    },
    4056: {
        name: 'Shanks v2 6+ (~ 6am)'
    },
    4079: {
        name: 'Alber (~ 6am)'
    },
    4098: {
        name: 'Rayleigh v3 6+ (~ 6am)'
    },
    4120: {
        name: 'X Drake (~ 6am)'
    },
    4139: {
        name: 'Bonney 6+ (~ 6am)'
    },
    4131: {
        name: 'Luffy (Egghead) (~ 6am)',
        xch_id: 4170
    },
    4185: {
        name: 'Law v3 6+ (~ 6am)'
    },
    4201: {
        name: 'Moria (~ 6am)',
        xch_id: 4199
    },
    4217: {
        name: 'Sakazuki v3 6+ (~ 6am)',
        xch_id: 4209
    },
    4237: {
        name: 'Hawkins 6+ (~ 6am)'
    },
    4256: {
        name: 'Magellan v2 6+ (~ 6am)'
    },
    4274: {
        name: 'Croc v2 6+ (~ 6am)'
    },
    4287: {
        name: 'Sanji & Pudding 6+ (~ 6am)'
    },
    4306: {
        name: 'Kuma 6+ (~ 6am)',
        xch_id: 4263
    },
    4321: {
        name: 'Kuzan v3 6+ (~ 6am)',
        xch_id: 4318
    },
};
