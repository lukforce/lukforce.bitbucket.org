var pkas = {
    3885: {
        name: 'Roger'
    },
    3927: {
        name: 'Sanji'
    },
    3952: {
        name: 'Maha'
    },
    3976: {
        name: 'O-Tama'
    },
    3999: {
        name: 'Gordon'
    },
    4025: {
        name: 'Blueno & Bepo & Sunny-kun'
    },
    4047: {
        name: 'Luffy'
    },
    4066: {
        name: 'Edward Newgate'
    },
    4088: {
        name: 'Jinbe'
    },
    4107: {
        name: 'Coby'
    },
    4127: {
        name: 'CP-0'
    },
    4146: {
        name: 'Blackbeard'
    },
    4176: {
        name: 'Mihawk'
    },
    4190: {
        name: 'Arlong'
    },
    4205: {
        name: 'Ace'
    },
    4224: {
        name: 'Bepo'
    },
    4245: {
        name: 'Kuzan'
    },
    4260: {
        name: 'Dr. Vegapunk'
    },
    4279: {
        name: 'Orochi & Kanjuro'
    },
    4297: {
        name: 'Akainu'
    },
    4311: {
        name: 'S-Shark'
    },
    4326: {
        name: 'S-Bear'
    },
};
