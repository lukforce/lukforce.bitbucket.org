var tms = {
    1808: {
        name: 'Mihawk',
        info: 'i.redd.it/1wlxtu6r3ql01.png'
    },
    1853: {
        name: 'Whitebeard',
        info: 'i.redd.it/604g03thb9r01.png'
    },
    1889: {
        name: 'Cavendish',
        info: 'i.imgur.com/hM13hHv.png'
    },
    1916: {
        name: 'Gear 4'
    },
    1941: {
        name: 'Ace',
        info: 'i.redd.it/pzbzhnv2bk411.png'
    },
    1972: {
        name: 'Valentine Sabo',
        info: 'i.imgur.com/Sd6BIUR.png'
    },
    2000: {
        name: 'Sanji & Zoro',
        info: 'i.imgur.com/S91geBt.png'
    },
    2064: {
        name: 'Kizaru',
        info: 'i.imgur.com/gw4s7Cr.png'
    },
    2109: {
        name: 'Big Mom',
        info: 'i.imgur.com/c17jXBm.jpg'
    },
    2137: {
        name: 'Crocodile',
        info: 'i.imgur.com/KeJBbsj.png'
    },
    2175: {
        name: 'Jack',
        info: 'i.imgur.com/RIR63CN.png'
    },
    2211: {
        name: 'Eneru',
        info: 'i.imgur.com/g6OUbLO.jpg'
    },
    2261: {
        name: 'Shanks',
        info: 'i.imgur.com/AiMEgJY.jpg'
    },
    2299: {
        name: 'Boa Hancock',
        info: 'i.imgur.com/wrLWAbA.png'
    },
    2336: {
        name: 'Law',
        info: 'i.imgur.com/GaFJQ8Y.jpg'
    },
    2362: {
        name: 'Christmas Nami',
        info: 'i.imgur.com/EwKiR7y.png'
    },
    2387: {
        name: 'Kaido',
        info: 'i.imgur.com/iTBw5FQ.jpg'
    },
    2443: {
        name: 'Doflamingo',
        info: 'i.imgur.com/shsApVp.png'
    },
    2469: {
        name: 'Smoothie & Oven',
        info: 'i.imgur.com/o5PWrbW.png'
    },
    2510: {
        name: 'Weevil',
        info: 'i.imgur.com/yd2O3of.png'
    },
    2557: {
        name: 'Shanks & Beckman'
    },
    2583: {
        name: 'Lucci'
    },
    2618: {
        name: 'Sakazuki & Issho',
        info: 'i.imgur.com/Ye3vCLH.png'
    },
    2659: {
        name: 'Smoker',
        info: 'i.imgur.com/CMGb9OQ.png'
    },
    2690: {
        name: 'Blackbeard (INT)',
        info: 'i.imgur.com/KtGlRXi.png'
    },
    2729: {
        name: 'Hawkins',
        info: 'i.imgur.com/XHWoQs9.png'
    },
    2763: {
        name: 'Halloween Nami',
        info: 'i.imgur.com/ABVTYHz.png'
    },
    2792: {
        name: 'O-Robi',
        info: 'i.imgur.com/q7DHvOq.png'
    },
    2823: {
        name: 'X Drake',
        info: 'i.imgur.com/cAxtgQe.png'
    },
    2850: {
        name: 'Rebecca & Viola',
        info: 'i.imgur.com/SvwUseW.png'
    },
    2879: {
        name: 'Vivi',
        info: 'i.imgur.com/puuo4dU.png'
    },
    2913: {
        name: 'Shirahoshi',
        info: 'i.imgur.com/QzeiQPO.png'
    },
    2977: {
        name: 'Anni Luffy',
        info: 'i.imgur.com/GpV102s.png'
    },
    2995: {
        name: 'Zorojuro',
        info: 'i.imgur.com/uB6GQSX.png'
    },
    3022: {
        name: 'Reiju',
        info: 'i.imgur.com/6gWnSzG.png'
    },
    3060: {
        name: 'Nami & Sanji',
        info: 'i.imgur.com/fGp35AD.png'
    },
    3094: {
        name: 'Jinbe'
    },
    3115: {
        name: 'Pudding',
        info: 'i.imgur.com/TkLUvMe.png'
    },
    3150: {
        name: 'Kid'
    },
    3197: {
        name: 'Garp'
    },
    3239: {
        name: 'Queen'
    },
    3273: {
        name: 'Luffy & Ace'
    },
    3297: {
        name: 'Shiryu'
    },
    3339: {
        name: 'Koala'
    },
    3368: {
        name: 'MUGIWARA 56 Zoro'
    },
    3389: {
        name: 'Dragon Kaido'
    },
    3416: {
        name: 'Stussy'
    },
    3527: {
        name: 'Tashigi (1am ~ 6am)',
        info: 'cdn.discordapp.com/attachments/798165270584885268/931603085078433812/tm_tashigi_.png'
    },
    3547: {
        name: 'Perospero (1am ~ 6am)'
    },
    3567: {
        name: 'Kanjuro (1am ~ 6am)'
    },
    3588: {
        name: 'Ulti (1am ~ 6am)'
    },
    3621: {
        name: 'Sabo (1am ~ 6am)'
    },
    3645: {
        name: 'Moria v2 (1am ~ 6am)'
    },
    3663: {
        name: 'Franky (1am ~ 6am)'
    },
    3689: {
        name: 'RED Sanji (1am ~ 6am)'
    },
    3712: {
        name: 'RED Chopper v2 (1am ~ 6am)'
    },
    3736: {
        name: 'Law v2 (1am ~ 6am)'
    },
    3758: {
        name: 'O-Lin (1am ~ 6am)'
    },
    3778: {
        name: 'Who\'s-Who (1am ~ 6am)'
    },
    3801: {
        name: 'Wyper (1am ~ 6am)'
    },
    3822: {
        name: 'Izo (1am ~ 6am)'
    },
    3842: {
        name: 'Fujitora (1am ~ 6am)'
    },
    3864: {
        name: 'Bege (1am ~ 6am)'
    },
    3894: {
        name: 'Momonosuke (1am ~ 6am)'
    },
    3915: {
        name: 'Akainu (1am ~ 6am)'
    },
    3936: {
        name: 'Flampe (1am ~ 6am)'
    },
    3965: {
        name: 'Yamato (1am ~ 6am)'
    },
    3981: {
        name: 'Lead Performers (1am ~ 6am)'
    },
    4006: {
        name: 'Baccarat & Dice & Mr. Tanaka (1am ~ 6am)'
    },
    4033: {
        name: 'Blueno & Bepo & Sunny-kun (1am ~ 6am)'
    },
    4053: {
        name: 'Luffy (1am ~ 6am)'
    },
    4074: {
        name: 'Edward Newgate (1am ~ 6am)'
    },
    4089: {
        name: 'Jinbe (1am ~ 6am)'
    },
    4108: {
        name: 'Coby (1am ~ 6am)'
    },
    4128: {
        name: 'CP0 (1am ~ 6am)'
    },
    4147: {
        name: 'Blackbeard (DEX) (1am ~ 6am)'
    },
    4177: {
        name: 'Mihawk (DEX) (1am ~ 6am)',
        info: 'fixupx.com/tingoki/status/1800849219416453435'
    },
    4191: {
        name: 'Arlong (1am ~ 6am)',
        info: 'fixupx.com/tingoki/status/1811061036537008366'
    },
    4206: {
        name: 'Ace (QCK) (1am ~ 6am)'
    },
    4225: {
        name: 'Bepo (1am ~ 6am)'
    },
    4246: {
        name: 'Kuzan (1am ~ 6am)'
    },
    4261: {
        name: 'Vegapunk (1am ~ 6am)'
    },
    4280: {
        name: 'Orochi & Kanjuro (1am ~ 6am)'
    },
    4298: {
        name: 'Sakazuki (QCK) (1am ~ 6am)'
    },
    4312: {
        name: 'S-Shark (1am ~ 6am)'
    },
    4327: {
        name: 'S-Bear (1am ~ 6am)'
    },
};
